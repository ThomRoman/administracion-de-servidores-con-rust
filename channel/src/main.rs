// crea un nuevo canal asincrono, devolviendo las mitades del transmisor y receptor
use std::sync::mpsc;
use std::thread;
use std::time::Duration;
/*

los traits Send y Sync capturan y controlan las dos formas mas comunes en las que se accede
a un dato y se lo arroja a los hilos

*/
fn main() {

    let (transmisorX,receptorX) = mpsc::channel();

    let transmisorX1  = mpsc::Sender::clone(&transmisorX);
    // let transmisorX1 = transmisorX.clone();
    thread::spawn(move || {
        let vals = vec![
            String::from("hola"),
            String::from("desde"),
            String::from("el"),
            String::from("hilo 1"),
        ];
        for val in vals {
            transmisorX1.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    thread::spawn(move || {
        let vals = vec![
            String::from("vemos"),
            String::from("como"),
            String::from("trabaja"),
            String::from("hilo 2"),
        ];
        for val in vals {
            transmisorX1.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    for received in receptorX {
        println!("Obtener mensaje {}",received);
    }

    println!("Hello, world!");
}
