use std::thread; // nos facilitara para la creacion de hilos
use std::time::Duration;

fn main() {
    // spawn es una funcion generica con iun argumento y dos parametros de tipo
    // fn spawn<F,T>(f:F)->JoinHandle<T>
    /**
    F es un tipo de funcion, puede ser cuaquier funcion siempre y cuando devulva 
    un valor de tipo T, se puede transmitir de forma seguro entre subprocesos (send)
    y no contiene referencias de corta duracion ('static)

    el tipo t puede ser de cualquier tipo, siempre que se pueda transferir entre subprocesos
    y no contenga referencias de corta duracion

    El JoinHandle<T> permite recuperar el T que F regresa, a traves de su metodo join
    
    el move es un cierre de hilos que a menudo se usa junto a thread::spawn porque le permite
    usar datos de un hilo a otro hilo
    thread::spawn(move || {})

    */
    let handle = thread::spawn(|| {
        for i in 1..10{
            println!("Hola {} desde el primer hilo",i);

            // obligan a un subproceso a detener su ejecucion durante un periodo breve
            // lo que permite que se ejecute un subproceso diferente.
            // los subprocesos probablemente e turne, pero eso no está garantindado, depende
            // de como sus sistema operativo programe los subprocesos
            thread::sleep(Duration::from_millis(i));

        }
    });
    let handle1 = thread::spawn(|| {
        for i in 1..10{
            println!("Hola {} desde el segundo hilo",i);
            thread::sleep(Duration::from_millis(i));

        }
    });

    let handle2 = thread::spawn(|| {
        for i in 1..10{
            println!("Hola {} desde el tercer hilo",i);
            thread::sleep(Duration::from_millis(i));

        }
    });  

    handle.join().unwrap();
    handle1.join().unwrap();
    handle2.join().unwrap();
}
