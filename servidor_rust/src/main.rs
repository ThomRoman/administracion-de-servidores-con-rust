/*
Comenzamos llamando al modulo std::net y sus metodos TcpStream TcpListener
*/

use std::io::prelude::*;
use std::net::{TcpStream,TcpListener};
use std::fs; // nos ayudara a leer un archivo como una cadena de caracteres

fn main() {
    // println!("Hello, world!");

    // en este caso estamos dando valor a una variable donde por medio de la struct de TcpListener
    // (una structura que representa un socket server) le decimos que escuche desde Tcp conexion
    // localhost o 127.0.0.1:7373, para esto la funcion bind nos ayuda a decirle que esta es una nueva isntacion
    // A diferencia de otros lenguajes no podemos usar un puerto como 80
    // ya que necesitamos credenciales de administraccion
    // es por eso que nuestros puertos deben ser mayor a 1024.
    // con el unwrap le decirmos que si hay algun error que termine la conexion


    // TcpStream es una estructura que reprensenta una secuencia TCP
    // entre un socket local y un socket remoto,es decir ve si hay conexion entre cliente y servidor
    let listener = TcpListener::bind("127.0.0.1:7373").unwrap();


    // el stream es una version asincrona de un iterador, un stream representa una conexion abierta
    // entre el cliente y el servidor,
    // incoming es un iterador de TcpListenes que nos devuelve un mismo iterador que nos da
    // una secuencia de secuencias
    for stream in listener.incoming(){
        // match stream{
        //     Ok(stream) =>{
        //         println!("¡Nuevo cliente!")
        //     }
        //     Err(error)=>{
        //         println!("Conexion fallida")
        //     }
        // }
        let stream = stream.unwrap();
        handle_connection(stream);
    }
}
// el parametro es mutable ya que no sabemos si podria leer mas datos de los que solicitamos 
// y guardarlos despues
fn handle_connection(mut stream:TcpStream){
    // para poder leer el request y ver datos dentro de nuestro navegador

    // agregamos un buffer a nuestro servidor con un tamaño de 256 bytes
    // let mut buffer = [0; 256];
    // si esto no funciona cambiar por [0; 1024]
    let mut buffer = [0; 1024];

    //  el método stream.read se encargara de leer todos los bytes recibidos
    stream.read(&mut buffer).unwrap();


    let get = b"GET / HTTP/1.1\r\n"; //respuesta de nuestra solicitud
    // debido a que estamos leyendo bytes sin procesar en el buffer, transformamos en una
    // cadena de bytes

    // esto significa que el buffer recibe una respuesta en get y nuestra condicion
    // se cumple, quiere decir que tendremos que arrojar nuestro emnsjae con el 
    // 200
    if buffer.starts_with(get){
        let status_line = header_http_message(200);
        let content = fs::read_to_string("index.html").unwrap();
        web_response(stream,content,status_line.to_string());
    }else{
        let status_line = header_http_message(404);
        let content = fs::read_to_string("error.html").unwrap();
        web_response(stream,content,status_line.to_string());
    }
}


fn web_response(mut stream:TcpStream,content : String,status_line: String){
    let response = format_reponse(status_line.to_string(),content.to_string());
    stream.write(response.as_bytes()).unwrap();
    stream.flush().unwrap();
}

fn format_reponse(status_line: String,content : String)->String{
    return format!(
        "{}\r\nContent-Length: {}\r\n\r\n{}",
        status_line,
        content.len(),
        content
    );
}

fn header_http_message( code : i64 )->String{
 let status_message = match code {
     200 => "OK",
     404 => "NOT FOUND",
     _ => "Error"
 };

 return format!("HTTP/1.1 {} {}",code,status_message);
}
