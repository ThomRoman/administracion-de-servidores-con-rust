# **Administracion de servidores con Rust**

## **Instalacion de Rust**

```sh
    $ curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

- Luego tomamos la opcion 1 "Proceed with installation (default)"
- `source $HOME/.cargo/env`
- `rustc --version && cargo --version`

## **Conexion TCP**

TCP/HTTP ambos son protocolos de solicitud de respuesta, lo que significa
que un cliente inicia solicitudes y un servidor escucha las solicitudes y proporciona una respuesta.

TCP es un protocolo de bajo nivel, que se encarga de hacer la **conexion** que describe los 
detalles de **comunicacion entre el servidor y otro**. HTTP trabaja dentro de TCP y es
el principal encargado de las solicitudes y respuestas.

Como TCP es el encargado de manejar la conexion entre servidores, es lo primero que se
debe de configurar. Dentro de la liberia estandar que se encuentra en Rust, existe
**`std::net`** ***(este modulo proporciona funcionalidad de red para los protocolos de <br/> control de transmision y datagramas de usuario,así como los tipos de direcciones IP y socket)***
En este modulo se cuenta con dos metodos `TopStream` y `TopListener`

## **Creacion del proyecto**

```sh
    $ cargo new servidorRust
    $ cd servidorRust && cargo run
```

```rust
    /*
Comenzamos llamando al modulo std::net y sus metodos TcpStream TcpListener
*/

use std::io::prelude::*;
use std::net::{TcpStream,TcpListener};


fn main() {
    // println!("Hello, world!");

    // en este caso estamos dando valor a una variable donde por medio de la struct de TcpListener
    // (una structura que representa un socket server) le decimos que escuche desde Tcp conexion
    // localhost o 127.0.0.1:7373, para esto la funcion bind nos ayuda a decirle que esta es una nueva isntacion
    // A diferencia de otros lenguajes no podemos usar un puerto como 80
    // ya que necesitamos credenciales de administraccion
    // es por eso que nuestros puertos deben ser mayor a 1024.
    // con el unwrap le decirmos que si hay algun error que termine la conexion


    // TcpStream es una estructura que reprensenta una secuencia TCP
    // entre un socket local y un socket remoto,es decir ve si hay conexion entre cliente y servidor
    let listener = TcpListener::bind("localhost:7373").unwrap();


    // el stream es una version asincrona de un iterador, un stream representa una conexion abierta
    // entre el cliente y el servidor,
    // incoming es un iterador de TcpListenes que nos devuelve un mismo iterador que nos da
    // una secuencia de secuencias
    for stream in listener.incoming(){
        match stream{
            Ok(stream) =>{
                println!("¡Nuevo cliente!")
            }
            Err(error)=>{
                println!("Conexion fallida")
            }
        }
    }
}
```

Los mensajes HTTP son los medios por los cuales se intercambian datos entre servidores
y clientes. Hay dos tipos de mensajes: peticiones(request) y respuestas(response)

- request : enviadas por el cliente al servidor, para pedir el inicio de una accion
- response : la respuesta del servidor al cliente

`use std::io::prelude::*;` En este modulo tenemos el metodo `prelude` que nos ayudará a
leer, escribir y releer la respuesta que recibimos en bytes

Tambien crearemos una funcion llamada `handle_connection`, aqui leeremos los datos del flujo
TCP y los imprimiremos para ver los datos se se envian desde el navegador

```rust
use std::io::prelude::*;
/*
Comenzamos llamando al modulo std::net y sus metodos TcpStream TcpListener
*/

use std::io::prelude::*;
use std::net::{TcpStream,TcpListener};


fn main() {
    let listener = TcpListener::bind("localhost:7373").unwrap();
    for stream in listener.incoming(){
        // match stream{
        //     Ok(stream) =>{
        //         println!("¡Nuevo cliente!")
        //     }
        //     Err(error)=>{
        //         println!("Conexion fallida")
        //     }
        // }
        let stream = stream.unwrap();
        handle_connection(stream);
    }
}

// el parametro es mutable ya que no sabemos si podria leer mas datos de los que solicitamos 
// y guardarlos despues
fn handle_connection(mut stream:TcpStream){}
```

> Revisar el codigo, se agregara el mensaje 404 Page Not found

## **Concurrencia en Rust**

Concurrente : Donde diferentes partes de un programa se ejecutan de manera independiente
Paralela : donde diferente partes de un programa se ejecutan al mismo tiempo.

Los errores de seguridad de la memoria y los errores de concurrencia a menudo se reducen al
codigo de acceso a los datos cuando no deberia. El arma sercreta de rust es el **ownership**
una disciplina para el control de acceso que los programadores de sistemas intentan seguir

Ejemplos de concurrencia en Rust:

- Los canales de Rust imponen el aislamiendo del hilo
- "Bloquear datos, no codigo" se aplica en rust
- https://medium.com/@rcougil/software-paralelismo-y-concurrencia-ae6018f68d75
- https://www.oscarblancarteblog.com/2017/03/29/concurrencia-vs-paralelismo/ 

Una herramienta importante que rust tiene para lograr la concurrencia de envio de mensajes 
es el `channel`.

Los traits en rust son como interface en otros lenguajes

## Referencias

- [Crates](https://crates.io/)
- [Learn Rust](https://www.rust-lang.org/learn/get-started)
- [Rust Playground](https://play.rust-lang.org/)
- [Mi propia guia cuando aprendi Rust](https://gitlab.com/ThomRoman/learning-rust)